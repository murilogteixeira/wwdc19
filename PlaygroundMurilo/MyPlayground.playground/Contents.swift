/*:
 
 # Playground name: VALUES
 
 ## Description
 
 There are many things we do every day that sometimes go unnoticed and do not act the right way. To show which are some of these attitudes, the scenes can express well what to do in certain situations. Thinking about it, this playground was developed based on positive and negative decisions, showing that we can always do different things and make the right decisions to make life more harmonious. The goal is to teach some values ​​so that the relationship between people becomes even more friendly and that, with simple attitudes, we can make someone's day better than it was yesterday.
 
 The values ​​taught are based on what I have learned from my parents since childhood and I have also learned throughout my life from my own experiences.
 
 ## How to play
 
 To play, simply review the question displayed at the top of the screen and respond between the A or B answers displayed at the bottom of the screen. If the player responds to something that is not the best option between them, he should try again until the correct alternative is selected. There is no game over, because in this playground the goal is not to make the player lose the round, but to learn good manners for a lifetime.
 
 ## Credits
 
 All the code was developed by me - **Murilo Gonçalves Teixeira**.
 
 All images used are free for commercial use.
 
 The origin of each image is described below:
 1. coffee-break-1177540_1280.png - [https://pixabay.com/photos/coffee-break-conference-women-1177540/](https://pixabay.com/photos/coffee-break-conference-women-1177540/)
 2. couple-2603716_1920.png - [https://pixabay.com/photos/couple-man-woman-wedding-love-2603716/](https://pixabay.com/photos/couple-man-woman-wedding-love-2603716/)
 3. cafe-1699126_1920.png - [https://pixabay.com/photos/cafe-waiter-operation-flirt-kundin-1699126/](https://pixabay.com/photos/cafe-waiter-operation-flirt-kundin-1699126/)
 4. crosswalk-2404427_1920.png - [https://pixabay.com/photos/crosswalk-zebra-pedestrian-street-2404427/](https://pixabay.com/photos/crosswalk-zebra-pedestrian-street-2404427/)
 5. apples-1841132_1920.png - [https://pixabay.com/photos/apples-farmers-market-business-buy-1841132/](https://pixabay.com/photos/apples-farmers-market-business-buy-1841132/)
 6. balloons-2826093_1280.png - [https://pixabay.com/photos/balloons-celebration-congratulation-2826093/](https://pixabay.com/photos/balloons-celebration-congratulation-2826093/)
 7. waiter-492872_1280.png - [https://pixabay.com/photos/waiter-bread-deliver-serve-food-492872/](https://pixabay.com/photos/waiter-bread-deliver-serve-food-492872/)
 8. passengers-1150043_1280.png - [https://pixabay.com/photos/passengers-tain-tram-bus-subway-1150043/](https://pixabay.com/photos/passengers-tain-tram-bus-subway-1150043/)
 9. taxi-1209542_1280.png - [https://pixabay.com/photos/taxi-vehicle-road-city-urban-cars-1209542/](https://pixabay.com/photos/taxi-vehicle-road-city-urban-cars-1209542/)
 */

import PlaygroundSupport
import SpriteKit

let width = 667
let height = 375

let initialView = SKView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: height)))
let initialScene = GameScene_1(size: initialView.frame.size)
initialScene.scaleMode = .aspectFill
initialView.presentScene(initialScene)
PlaygroundPage.current.liveView = initialView

