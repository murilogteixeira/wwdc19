import SpriteKit
import PlaygroundSupport

public class GameScene_3 : SKScene {
    
    var background = SKSpriteNode(imageNamed: "cafe-1699126_1920")
    var msgSpace: SKShapeNode?
    var msg = SKLabelNode(text: "How should you order something from a restaurant?")
    var buttonTrue: SKShapeNode?
    var buttonFalse: SKShapeNode?
    var positivePhrase = "Please, could you give me a drink?"
    var negativePhrase = "I Want a Drink!"
    var answer1 = SKLabelNode(text: "")
    var answer2 = SKLabelNode(text: "")
    var alertBackground: SKSpriteNode?
    var alertShape: SKShapeNode?
    var alertMsgTrue = "Right! Education is essential, always."
    var alertMsgFalse = "Come on. You may be more educated!"
    var alertMsg = SKLabelNode(text: "")
    var alertButton = SKShapeNode()
    var alertButtonTextTrue = "Next"
    var alertButtonTextFalse = "Try again"
    var alertButtonText = SKLabelNode(text: "")
    let fontName = "bradley hand"
    let fontNameButtons = "arial rounded mt bold"
    
    public override init(size: CGSize) {
        super.init(size: size)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func didMove(to view: SKView) {
        
        background.size = frame.size
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(background)
        
        msgSpace = SKShapeNode(rectOf: CGSize(width: 642, height: 70), cornerRadius: CGFloat(10))
        setVisualDialog(shape: msgSpace!)
        msgSpace?.position = CGPoint(x: frame.midX, y: 325)
        addChild(msgSpace!)
        
        msg.fontName = fontName
        msg.fontColor = .black
        msg.verticalAlignmentMode = .center
        msg.fontSize = 24.5
        msg.preferredMaxLayoutWidth = 600
        msg.numberOfLines = 2
        msgSpace?.addChild(msg)
        
        buttonTrue = SKShapeNode(rectOf: CGSize(width: 308, height: 70), cornerRadius: CGFloat(10))
        setVisualButton(shape: buttonTrue!)
        addChild(buttonTrue!)
        
        buttonFalse = SKShapeNode(rectOf: CGSize(width: 308, height: 70), cornerRadius: CGFloat(10))
        setVisualButton(shape: buttonFalse!)
        addChild(buttonFalse!)
        
        if(Bool.random()){
            buttonTrue?.position = CGPoint(x: 170, y: 50)
            buttonFalse?.position = CGPoint(x: 497, y: 50)
            answer1.text = "A) \(positivePhrase)"
            answer2.text = "B) \(negativePhrase)"
        }else{
            buttonFalse?.position = CGPoint(x: 170, y: 50)
            buttonTrue?.position = CGPoint(x: 497, y: 50)
            answer1.text = "B) \(positivePhrase)"
            answer2.text = "A) \(negativePhrase)"
        }
        
        enableButtons(button1: buttonTrue!, button2: buttonFalse!)
        
        answer1.fontName = fontNameButtons
        answer1.fontColor = .white
        answer1.verticalAlignmentMode = .center
        answer1.fontSize = 20.0
        answer1.numberOfLines = 2
        answer1.preferredMaxLayoutWidth = 250
        buttonTrue?.addChild(answer1)
        
        answer2.fontName = fontNameButtons
        answer2.fontColor = .white
        answer2.verticalAlignmentMode = .center
        answer2.fontSize = 20.0
        answer2.numberOfLines = 2
        answer2.preferredMaxLayoutWidth = 250
        buttonFalse?.addChild(answer2)
        
        alertBackground = SKSpriteNode(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7957746479), size: frame.size)
        alertBackground?.position = CGPoint(x: frame.midX, y: frame.midY)
        alertBackground?.isHidden = true
        addChild(alertBackground!)
        
        alertShape = SKShapeNode(rectOf: CGSize(width: 500, height: 200), cornerRadius: CGFloat(10))
        alertShape?.position = CGPoint(x: frame.midX, y: frame.midY)
        alertShape?.isHidden = true
        addChild(alertShape!)
        
        
        
        alertMsg.fontName = fontName
        alertMsg.preferredMaxLayoutWidth = 450
        alertMsg.numberOfLines = 2
        alertMsg.isHidden = true
        alertShape?.addChild(alertMsg)
        
        alertButton = SKShapeNode(rectOf: CGSize(width: 200, height: 50), cornerRadius: 10)
        alertButton.position = CGPoint(x: frame.midX, y: frame.midY-60)
        alertButton.isHidden = true
        addChild(alertButton)
        
        alertButtonText = SKLabelNode(fontNamed: fontNameButtons)
        alertButtonText.fontSize = 24
        alertButtonText.verticalAlignmentMode = .center
        alertButtonText.isHidden = true
        alertButton.addChild(alertButtonText)
        
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let positionInScene = touch.location(in: self)
            
            if(buttonTrue!.contains(positionInScene) && !buttonTrue!.isUserInteractionEnabled){
                showAlert(textMsg: alertMsgTrue, textButton: alertButtonTextTrue, next: true)
            }
            
            if(buttonFalse!.contains(positionInScene) && !buttonFalse!.isUserInteractionEnabled){
                showAlert(textMsg: alertMsgFalse, textButton: alertButtonTextFalse, next: false)
                disableButtons(button1: buttonTrue!, button2: buttonFalse!)
            }
            
            if(alertButton.name == "Next" && alertButton.contains(positionInScene) && !alertButton.isUserInteractionEnabled){
                nextScene()
            }
            
            if(alertButton.name == "Try again" && alertButton.contains(positionInScene) && !alertButton.isUserInteractionEnabled){
                hiddenAlert()
                enableButtons(button1: buttonTrue!, button2: buttonFalse!)
            }
        }
    }
    
    public func nextScene(){
        
        let fade = SKTransition.fade(withDuration: 1.0)
        let nextScene = GameScene_4(size: frame.size)
        self.view?.presentScene(nextScene, transition: fade)
        
    }
    
    public func disableButtons(button1: SKShapeNode, button2: SKShapeNode){
        button1.isUserInteractionEnabled = true
        button2.isUserInteractionEnabled = true
    }
    
    public func enableButtons(button1: SKShapeNode, button2: SKShapeNode){
        button1.isUserInteractionEnabled = false
        button2.isUserInteractionEnabled = false
    }
    
    private func hiddenAlert(){
        alertBackground?.isHidden = true
        alertShape?.isHidden = true
        alertMsg.isHidden = true
        alertButton.isHidden = true
        alertButton.isUserInteractionEnabled = true
        alertButtonText.isHidden = true
    }
    
    private func showAlert(textMsg: String, textButton: String, next: Bool){
        
        switch next {
        case true:
            alertShape?.fillColor =  #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            alertShape?.strokeColor =  #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            alertShape?.lineWidth = CGFloat(2)
            
            alertButton.fillColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            alertButton.strokeColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
            alertButton.lineWidth = CGFloat(2)
        default:
            alertShape?.fillColor =  #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
            alertShape?.strokeColor =  #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            alertShape?.lineWidth = CGFloat(2)
            
            alertButton.fillColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            alertButton.strokeColor = #colorLiteral(red: 0.5633802817, green: 0.1491314173, blue: 0, alpha: 1)
            alertButton.lineWidth = CGFloat(2)
        }
        
        alertMsg.text = textMsg
        alertButtonText.text = textButton
        alertButton.name = textButton
        
        alertBackground?.isHidden = false
        alertShape?.isHidden = false
        alertMsg.isHidden = false
        alertButton.isHidden = false
        alertButton.isUserInteractionEnabled = false
        alertButtonText.isHidden = false
    }
    
    private func setVisualButton (shape: SKShapeNode) {
        
        shape.fillColor =  #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        shape.strokeColor =  #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        shape.lineWidth = CGFloat(2)
    }
    
    private func setVisualDialog (shape: SKShapeNode) {
        
        shape.fillColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        shape.strokeColor =  #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        shape.lineWidth = CGFloat(2)
    }
}
