import UIKit
import SpriteKit
import PlaygroundSupport

public class GameScene_9: SKScene {
    
    var background = SKSpriteNode(imageNamed: "balloons-2826093_1280")
    var board: SKShapeNode?
    var msgBoard: SKLabelNode?
    var button: SKShapeNode?
    var buttonText: SKLabelNode?
    
    public override func didMove(to view: SKView) {
        
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        background.size =  frame.size
        addChild(background)
        
        board = SKShapeNode(rectOf: CGSize(width: width-25, height: height-25), cornerRadius: CGFloat(10))
        board?.fillColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8)
        board?.position = CGPoint(x: width/2, y: height/2)
        addChild(board!)
        
        msgBoard = SKLabelNode(fontNamed: "bradley hand")
        msgBoard?.text = "CONGRATULATIONS!!\nYou have learned good manners of: Kindness, Education, Empathy, and Generosity.\nYou can always be the best version of yourself.\nI count on you !!"
        msgBoard?.numberOfLines = 2
        msgBoard?.preferredMaxLayoutWidth = 550
        msgBoard?.fontSize = 35.0
        msgBoard?.fontColor = .black
        msgBoard?.verticalAlignmentMode = .center
        msgBoard?.lineBreakMode = .byWordWrapping
        msgBoard?.numberOfLines = 2
        board?.addChild(msgBoard!)
        
    }
}
