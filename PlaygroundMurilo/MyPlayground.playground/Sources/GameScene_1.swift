import UIKit
import SpriteKit
import PlaygroundSupport
 
let width = 667
let height = 375

public class GameScene_1 : SKScene {
    
    var background = SKSpriteNode(imageNamed: "coffee-break-1177540_1280")
    var board: SKShapeNode?
    var msgBoard: SKLabelNode?
    var button: SKShapeNode?
    var buttonText: SKLabelNode?
    
    public override func didMove(to view: SKView) {
        
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        background.size =  frame.size
        addChild(background)
        
        board = SKShapeNode(rectOf: CGSize(width: width-25, height: height-25), cornerRadius: CGFloat(10))
        board?.fillColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.764084507)
        board?.position = CGPoint(x: width/2, y: height/2)
        addChild(board!)
        
        msgBoard = SKLabelNode(fontNamed: "bradley hand")
        msgBoard?.text = "Want to learn how to have good attitudes in your relationships?"
        msgBoard?.numberOfLines = 2
        msgBoard?.preferredMaxLayoutWidth = 450
        msgBoard?.fontSize = 30.0
        msgBoard?.fontColor = .black
        board?.addChild(msgBoard!)
        
        let fillColor =  #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        let strokeColor =  #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        let lineWidth = CGFloat(2)
        
        button = SKShapeNode(rect: CGRect(x: frame.midX - 100, y: frame.midY - 150, width: 200, height: 70), cornerRadius: CGFloat(10))
        button?.fillColor = fillColor
        button?.strokeColor = strokeColor
        button?.lineWidth = lineWidth
        addChild(button!)
        
        buttonText = SKLabelNode(fontNamed: "arial rounded mt bold")
        buttonText?.text = "Let`s Go!"
        buttonText?.fontColor = .black
        buttonText?.position = CGPoint(x: frame.midX, y: frame.midY - 130)
        
        button?.addChild(buttonText!)
        
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self)
            if (button!.contains(position)){
                let fade = SKTransition.fade(withDuration: 1.0)
                let nextScene = GameScene_2(size: frame.size)
                self.view?.presentScene(nextScene, transition: fade)
            }
        }
    }

    
}
